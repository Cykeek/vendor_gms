# Gapps

Google Mobile Applications for vanilla based Builds

## How to use

There are two scripts config.mk and config_mini.mk

To add Full Gapps put this line in device.mk or rom_device.mk

```bash
$(call inherit-product, vendor/gapps/config.mk)
```
To add mini Gapps put this line in device.mk or rom_device.mk

```bash
$(call inherit-product, vendor/gapps/config_mini.mk)
```


## Contributing

Thanks to [Hungphan](https://github.com/hungphan2001) for updating Sources to Android 13.
